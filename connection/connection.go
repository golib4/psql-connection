package pgsql

import (
	"database/sql"
	"fmt"
	"time"

	_ "github.com/jackc/pgx/v4/stdlib"
)

type Params struct {
	Host     string
	User     string
	Password string
	DbName   string
	Port     int32
	SslMode  string

	MaxOpenConns    int
	MaxIdleConns    int
	ConnMaxLifetime time.Duration
}

type Connection struct {
	connection *sql.DB
}

func NewConnection(params Params) (*Connection, error) {
	dataSourceName := fmt.Sprintf(
		"host=%s port=%d user=%s dbname=%s sslmode=%s",
		params.Host,
		params.Port,
		params.User,
		params.DbName,
		params.SslMode,
	)

	if params.Password != "" {
		dataSourceName = dataSourceName + fmt.Sprintf(" password=%s", params.Password)
	}

	connection, err := sql.Open("pgx", dataSourceName)
	if err != nil {
		return nil, fmt.Errorf("can not connect to db: %s", err)
	}

	err = connection.Ping()
	if err != nil {
		return nil, fmt.Errorf("db connection problem: %s", err)
	}

	connection.SetMaxOpenConns(params.MaxOpenConns)
	connection.SetMaxIdleConns(params.MaxIdleConns)
	connection.SetConnMaxLifetime(params.ConnMaxLifetime)

	return &Connection{connection: connection}, nil
}

func (c *Connection) GetConnection() *sql.DB {
	return c.connection
}
